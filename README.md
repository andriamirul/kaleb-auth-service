## Kaleb Auth Service

Kaleb Auth Service is a NestJS-based authentication service designed to manage user authentication and authorization. It provides endpoints for user registration, login, and profile management, leveraging JWT for secure token-based authentication.

### Getting Started

1. **Set up the environment**: Copy the `example.env` file to `.env` and configure the database host and other environment variables to match your local setup.
   `cp example.env .env`
2. Execute the `kaleb.sql` file located in the root directory to set up the database.
3. Install the required packages by running:
    ```yarn install```
4. Start the project with: 
    ```yarn start```
5. Access the Swagger documentation at `http://localhost:3000/api` to explore and interact with the Library Management and Algorithm Test APIs.

### Running Unit Tests

- You can run unit tests for the project using the following command: `yarn test`