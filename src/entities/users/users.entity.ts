import { Exclude } from 'class-transformer';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
} from 'typeorm';
import { BaseEntity } from '../base-entity';

@Entity({ name: 'users' })
export class UsersEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', length: 255, nullable: false })
    name: string;

    @Column({ type: 'varchar', length: 255, nullable: false, unique: true })
    username: string;

    @Column({ type: 'varchar', length: 255, nullable: false, })
    password: string;

    @Column({ type: 'varchar', length: 255, nullable: false })
    salt: string;
}
