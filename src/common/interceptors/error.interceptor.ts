import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  BadGatewayException,
  CallHandler,
  HttpException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((err) => {
        return throwError(() => {
          console.log(`test, ${typeof err}`);
          throw err;
          //   return new HttpException(
          //     {
          //       status: HttpStatus.FORBIDDEN,
          //       error: 'This is a custom exception message',
          //     },
          //     HttpStatus.FORBIDDEN,
          //   );
        });
      }),
    );
  }
}
