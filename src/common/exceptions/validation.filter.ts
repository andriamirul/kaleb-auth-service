import {
  ArgumentsHost,
  BadRequestException,
  Catch,
  ExceptionFilter,
} from '@nestjs/common';

export class ValidationException extends BadRequestException {
  public message;
  public validationErrors;

  constructor(validationErrors: any, message?: any) {
    super();
    this.message = message;
    this.validationErrors = validationErrors;
  }
}

@Catch(ValidationException)
export class ValidationFilter implements ExceptionFilter {
  catch(exception: ValidationException, host: ArgumentsHost): any {
    let message = exception.message ? exception.message : 'Invalid Fields';
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    return response.status(422).json({
      success: false,
      statusCode: 422,
      message: message,
      data: exception.validationErrors,
    });
  }
}
