import { Catch, ExceptionFilter, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';

@Catch(Error)
export class CustomExceptionFilter implements ExceptionFilter {
  public catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    return response.status(500).json({
      success: false,
      statusCode: 500,
      message: 'Internal server error',
      data: {
        shortMessage: exception.message,
        stack: exception.stack,
      },
    });
  }
}
