import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { HttpException, HttpStatus } from '@nestjs/common';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UsersEntity } from 'src/entities/users/users.entity';
import * as bcrypt from 'bcrypt';
import { DataSource, Repository } from 'typeorm';

describe('AuthService', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;
  let dataSource: DataSource;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
        JwtService,
        {
          provide: DataSource,
          useValue: {
            createQueryRunner: jest.fn().mockReturnValue({
              connect: jest.fn(),
              startTransaction: jest.fn(),
              commitTransaction: jest.fn(),
              rollbackTransaction: jest.fn(),
              release: jest.fn(),
              manager: {
                query: jest.fn(),
              },
            }),
          },
        },
        {
          provide: getRepositoryToken(UsersEntity),
          useClass: Repository,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
    dataSource = module.get<DataSource>(DataSource);
  });

  describe('login', () => {
    it('should return an object with an access token if the user is valid', async () => {
      const user = { username: 'andriamirul', id: 2, password: 'Coba12345' };
      jest.spyOn(authService, 'validateUser').mockImplementation(async () => user);
      jest.spyOn(jwtService, 'sign').mockImplementation(() => 'test_token');

      const result = await authService.login('andriamirul', 'Coba12345');
      expect(result).toHaveProperty('access_token');
      expect(result.access_token).toEqual(expect.any(String));
    });

    it('should throw an exception if user is not valid', async () => {
      jest.spyOn(authService, 'validateUser').mockImplementation(async () => null);

      await expect(authService.login('test', 'test')).rejects.toThrow(HttpException);
    });
  });
});
