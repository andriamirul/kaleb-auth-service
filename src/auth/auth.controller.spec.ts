import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { HttpException, HttpStatus } from '@nestjs/common';
import { LoginDto } from 'src/utils/dto/user/login-dto';

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn(),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  describe('login', () => {
    it('should return access token', async () => {
      const result = { access_token: 'test_token' };
      jest.spyOn(authService, 'login').mockImplementation(async () => result);

      expect(await authController.login({ username: 'test', password: 'test' } as LoginDto)).toBe(result);
    });

    it('should throw an exception', async () => {
      jest.spyOn(authService, 'login').mockImplementation(async () => {
        throw new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED);
      });

      await expect(authController.login({ username: 'wrong', password: 'wrong' } as LoginDto)).rejects.toThrow(
        new HttpException('Unauthorized', HttpStatus.UNAUTHORIZED),
      );
    });
  });
});
