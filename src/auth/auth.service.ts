import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import * as bcrypt from 'bcrypt';
import { DataSource } from 'typeorm';
import { UsersEntity } from 'src/entities/users/users.entity';

@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ) { }

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.usersService.findOne(username);
        if (!user) {
            return null
        }

        const hashedPassword = await bcrypt.hash(password, user.salt);
        if (!bcrypt.compare(hashedPassword, user.password)) {
            return null
        }

        await this.usersService.updatePassword(user, password)
        return user
    }

    async login(username: string, password: string) {
        try {
            const user = await this.validateUser(username, password)

            const payload = { username: user.username, sub: user.id };
            return {
                access_token: this.jwtService.sign(payload),
            };

        } catch (err) {
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR
            throw new HttpException(err, errorCode);
        }

    }
}
