import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CustomExceptionFilter } from './common/exceptions/custom-exception.filter';
import { CustomHttpExceptionFilter } from './common/exceptions/custom-http-exception.filter';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ValidationException, ValidationFilter } from './common/exceptions/validation.filter';
import { EntityNotFoundExceptionFilter } from './common/exceptions/not-found.filter';
import { SuccessInterceptor } from './common/interceptors/success.interceptor';
import { ValidationError } from 'class-validator';
import * as cors from 'cors';

function initCustomValidationError(app: INestApplication) {
  app.useGlobalFilters(
    new CustomExceptionFilter(),
    new CustomHttpExceptionFilter(),
    new ValidationFilter(),
    new EntityNotFoundExceptionFilter(),
  );
  app.useGlobalInterceptors(new SuccessInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      stopAtFirstError: true,
      skipMissingProperties: false,
      exceptionFactory: (errors: ValidationError[]) => {
        const errMsg = {};
        errors.forEach((err) => {
          errMsg[err.property] = Object.values(err.constraints)[0];
        });
        return new ValidationException(errMsg);
      },
    }),
  );
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(cors({
    origin: '*',
    allowedHeaders: 'Content-Type,Authorization',
  }));

  const options = new DocumentBuilder()
    .setTitle('Kaleb Auth Service')
    .setDescription('For user authentication')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
  initCustomValidationError(app);

  await app.listen(3000);
}

bootstrap();
