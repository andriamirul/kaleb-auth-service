import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { UsersEntity } from 'src/entities/users/users.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UsersEntity)
        private readonly usersRepository: Repository<UsersEntity>,
        private readonly dataSource: DataSource,
        private readonly jwtService: JwtService,
    ) { }

    async create(name: string, username: string, password: string) {
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const salt = await bcrypt.genSalt();
            const hashedPassword = await bcrypt.hash(password, salt);
            const user = queryRunner.manager.create(UsersEntity, {
                name,
                username,
                password: hashedPassword,
                salt
            });
            await queryRunner.manager.save(user);
            await queryRunner.commitTransaction();

            const payload = { username: user.username, sub: user.id };
            return {
                access_token: this.jwtService.sign(payload),
            };
        } catch (err) {
            await queryRunner.rollbackTransaction();
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR;
            throw new HttpException(err.message || 'Failed to create user', errorCode);
        } finally {
            await queryRunner.release();
        }
    }

    async updatePassword(user: UsersEntity, password: string): Promise<void> {
        const queryRunner = this.dataSource.createQueryRunner();
        await queryRunner.connect();
        await queryRunner.startTransaction();

        try {
            const newSalt = await bcrypt.genSalt();
            const newPassword = await bcrypt.hash(password, newSalt);
            await queryRunner.manager.update(UsersEntity, user.id, {
                password: newPassword,
                salt: newSalt
            });
            await queryRunner.commitTransaction();
        } catch (err) {
            await queryRunner.rollbackTransaction();
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR;
            throw new HttpException(err.message || 'Failed to update password', errorCode);
        } finally {
            await queryRunner.release();
        }
    }

    async findOne(username: string): Promise<UsersEntity> {
        try {
            const user = await this.usersRepository.findOne({ where: { username } });
            if (!user) {
                throw new HttpException('User not found', HttpStatus.NOT_FOUND);
            }
            return user;
        } catch (err) {
            const errorCode = err?.status ?? HttpStatus.INTERNAL_SERVER_ERROR;
            throw new HttpException(err.message || 'Error finding user', errorCode);
        }
    }
}
