import { Controller, Post, Body, Get, UseGuards, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RegisterDto } from 'src/utils/dto/user/register-dto';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';


@ApiTags('Users')
@Controller('users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    @Post('register')
    async register(@Body() req: RegisterDto) {
        return this.usersService.create(req.name, req.username, req.password);
    }

    @Get('profile')
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    async getProfile(@Request() req): Promise<any> {
        return {
            id: req.user.id,
            name: req.user.name,
            username: req.user.username
        }
    }
}
