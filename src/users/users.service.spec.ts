import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { UsersEntity } from 'src/entities/users/users.entity';
import { Repository, DataSource } from 'typeorm';
import { getRepositoryToken } from '@nestjs/typeorm';
import { JwtService } from '@nestjs/jwt';
import { HttpException, HttpStatus } from '@nestjs/common';

const mockUser = {
  id: 1,
  name: 'John Doe',
  username: 'johndoe',
  password: 'hashedPassword',
  salt: 'salt',
};

describe('UsersService', () => {
  let service: UsersService;
  let repository: Repository<UsersEntity>;
  let dataSource: DataSource;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(UsersEntity),
          useValue: {
            findOne: jest.fn().mockResolvedValue(mockUser),
            create: jest.fn().mockReturnValue(mockUser),
            save: jest.fn().mockResolvedValue(mockUser),
          },
        },
        {
          provide: DataSource,
          useValue: {
            createQueryRunner: jest.fn().mockReturnValue({
              connect: jest.fn(),
              startTransaction: jest.fn(),
              commitTransaction: jest.fn(),
              rollbackTransaction: jest.fn(),
              release: jest.fn(),
              manager: {
                create: jest.fn().mockReturnValue(mockUser),
                save: jest.fn().mockResolvedValue(mockUser),
              },
            }),
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn().mockReturnValue('jwt-token'),
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
    repository = module.get<Repository<UsersEntity>>(getRepositoryToken(UsersEntity));
    dataSource = module.get<DataSource>(DataSource);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create', () => {
    it('should create a new user and return a JWT token', async () => {
      const result = await service.create('John Doe', 'johndoe3', 'password');
      expect(result).toEqual({ access_token: expect.any(String) });
    });

    it('should throw an error if creating a user fails', async () => {
      jest.spyOn(dataSource.createQueryRunner().manager, 'save').mockRejectedValue(new Error('Failed to save'));

      await expect(service.create('John Doe', 'johndoe', 'password')).rejects.toThrow(
        new HttpException('Failed to save', HttpStatus.INTERNAL_SERVER_ERROR),
      );
    });
  });

  describe('findOne', () => {
    it('should find and return a user', async () => {
      const result = await service.findOne('johndoe');
      expect(result).toEqual(mockUser);
    });

    it('should throw an error if user is not found', async () => {
      jest.spyOn(repository, 'findOne').mockResolvedValue(null);

      await expect(service.findOne('johndoe')).rejects.toThrow(
        new HttpException('User not found', HttpStatus.NOT_FOUND),
      );
    });
  });
});
