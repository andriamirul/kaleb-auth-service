import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { JwtAuthGuard } from 'src/common/guards/jwt-auth.guard';
import { RegisterDto } from 'src/utils/dto/user/register-dto';
import { HttpException, HttpStatus } from '@nestjs/common';

const mockUser = {
  id: 1,
  name: 'John Doe',
  username: 'johndoe',
  password: 'hashedPassword',
  salt: 'salt',
};

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            create: jest.fn().mockResolvedValue({ access_token: expect.any(String) }),
            findOne: jest.fn().mockResolvedValue(mockUser),
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {
    it('should call UsersService.create and return a JWT token', async () => {
      const registerDto: RegisterDto = { name: 'John Doe', username: 'johndoe', password: 'password' };
      const result = await controller.register(registerDto);
      expect(result).toEqual({ access_token: expect.any(String) });
      expect(service.create).toHaveBeenCalledWith('John Doe', 'johndoe', 'password');
    });

    it('should throw an error if service.create fails', async () => {
      jest.spyOn(service, 'create').mockRejectedValue(new HttpException('Failed to create user', HttpStatus.INTERNAL_SERVER_ERROR));

      const registerDto: RegisterDto = { name: 'John Doe', username: 'johndoe', password: 'password' };

      await expect(controller.register(registerDto)).rejects.toThrow(
        new HttpException('Failed to create user', HttpStatus.INTERNAL_SERVER_ERROR),
      );
    });
  });

  describe('getProfile', () => {
    it('should return the user profile', async () => {
      const req = { user: mockUser };
      const result = await controller.getProfile(req);
      expect(result).toEqual({
        id: mockUser.id,
        name: mockUser.name,
        username: mockUser.username,
      });
    });
  });
});
