import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
    IsArray,
    IsBoolean,
    IsBooleanString,
    IsInt,
    IsNotEmpty,
    IsNumberString,
    IsOptional,
    IsString,
    Length,
} from 'class-validator';

export class PaginateDto {
    @IsNumberString()
    @IsNotEmpty()
    @ApiProperty()
    page: number;

    @ApiProperty()
    @IsNumberString()
    @IsNotEmpty()
    pageSize: number;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    filter: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    sortBy: string;

    @IsOptional()
    @IsString()
    @ApiPropertyOptional()
    orderBy: string;
}
